# OpenVPNConectClient
## This is the proprietary OpenVPN connect App.  
## Link: https://openvpn.net/vpn-client/
When a config file with all three connection options is imported it will only take the first one and stick with it.
If it is a FQDN it uses the IPv4 adress. 
When supplied with an IPv6 only config file it fails to connect with the following error:

> 
> ⏎[Oct 25, 2021, 12:25:24] EVENT: RECONNECTING ⏎[Oct 25, 2021, 12:25:24] EVENT: RESOLVE ⏎[Oct 25, 2021, 12:25:24]  Contacting [2a00:1398::76:70:6e:2]:1194 via UDP
> ⏎[Oct 25, 2021, 12:25:24] EVENT: WAIT ⏎[Oct 25, 2021, 12:25:24] WinCommandAgent: transmitting bypass route to > 2a00:1398::76:70:6e:2
> {
> 	"host" : "2a00:1398::76:70:6e:2",
> 	"ipv6" : true
> }
> 
> ⏎[Oct 25, 2021, 12:25:24] Transport Error: socket_protect error (UDP)
> ⏎[Oct 25, 2021, 12:25:24] Client terminated, restarting in 2000 ms...

This repeats until the max-connections value of 30 is reached.
IPv4 adress or FQDN with a valid A record work.
