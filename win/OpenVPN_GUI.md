#OpenVPN_GUI
##This is the open-source OpenVPN_GUI application
## Link: https://openvpn.net/community-downloads/
This app takes the first entry and ignores any further <connection> entries.
It successfully resolves the FQDN and connects both over IPv4 and IPv6 prefering IPv6.
No errors occur.
