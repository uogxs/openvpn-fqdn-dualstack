# Unofficial open-source client for android
## by Arne Schwabe
## Link:https://play.google.com/store/apps/details?id=de.blinkt.openvpn&hl=de&gl=US

The config file is imported successfully and ALL connections are stored not just the first:

<img src="https://git.scc.kit.edu/uogxs/openvpn-fqdn-dualstack/-/raw/main/android/1.jpg" alt="multiple entries" width="200"/>


Connection works fine with both protocols and FQDN.
When no FQDN is given it will try the first IP (IPv6 in my case) and fail with a network unreachable error.
It then proceeds to using the next entry (IPv4 in my case ) wich then works. So even without a FQDN dual stack 
is possible.
Reconnect:


<img src="https://git.scc.kit.edu/uogxs/openvpn-fqdn-dualstack/-/raw/main/android/2.jpg" alt="Reconnect" width="200"/>
