# Linux (Debian) Network Manager
## This uses the grafical Network Manager found in most Linux Distros.
When the config file is imported only the **last** <connection> entry is used.
If the last entry is the FQDN it successfully resolves it and connects fine using both protocols.
Otherwise it will just take the IP adress of the last <connection> entry. 
